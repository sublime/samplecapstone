import random


def listList(width, height, default=None):
    return [[default] * width for j in range(height)]

def around(x, y):
    yield x-1, y-1; yield x+0, y-1; yield x+1, y-1
    yield x-1, y+0
    # yield x+0, y+0 # skip the center
    yield x+1, y+0
    yield x-1, y+1; yield x+0, y+1; yield x+1, y+1
    
def getBasicConway():
    conway = Conway(width=25, height=5)
    conway.beehive(1, 1)
    conway.blinker(7, 1)
    conway.glider(11, 1)
    return conway

class Conway:
    def __init__(self, width, height):
        self.width = width
        self.height = height
        self.reset()

    def reset(self):
        self.grid = listList(self.width, self.height, False)
        self.numSteps = 0

    def __getitem__(self, xy):
        x, y = xy
        return self.grid[y][x]
    
    def __setitem__(self, xy, val):
        x, y = xy
        self.grid[y][x] = val

    def glider(self, x, y):
        self.grid[y+0][x+1] = True
        self.grid[y+1][x+2] = True
        self.grid[y+2][x+0] = True
        self.grid[y+2][x+1] = True
        self.grid[y+2][x+2] = True

    def beehive(self, x, y):
        self.grid[y+0][x+1] = True
        self.grid[y+0][x+2] = True
        self.grid[y+1][x+0] = True
        self.grid[y+1][x+3] = True
        self.grid[y+2][x+1] = True
        self.grid[y+2][x+2] = True

    def blinker(self, x, y):
        self.grid[y+0][x] = True
        self.grid[y+1][x] = True
        self.grid[y+2][x] = True

    def populate(self):
        pop = [self.glider, self.beehive, self.blinker]
        for x in range(1, self.width-4, 5):
            for y in range(2, self.height-4, 4):
                if random.randint(1,5) == 3: # gives some empty space
                    random.choice(pop)(x, y)

    def run(self, times=1):
        for _ in range(times):
            self._step()

    def iterXY(self):
        for x in range(self.width):
            for y in range(self.height):
                yield x, y

    def step(self, times=1):
        for _ in range(times):
            self._step()

    def _step(self):
        # Keep track of the # of living spaces around each space
        counts = listList(self.width, self.height, 0)
        for x, y in self.iterXY():
            if self.grid[y][x]: # if space is living, increase the nearby counts
                for i, j in around(x, y):
                    i = i % self.width
                    j = j % self.height
                    counts[j][i] += 1
        newGrid = listList(self.width, self.height, False)
        for x, y in self.iterXY():
            if counts[y][x] == 3: # Three nearby means grow life
                newGrid[y][x] = True
            elif counts[y][x] == 2: # Two nearby means just survive
                if self.grid[y][x]:
                    newGrid[y][x] = True
            # otherwise, the space stays dead
        self.grid = newGrid
        self.numSteps += 1

    def __str__(self):
        chars = listList(self.width, self.height, '  ')
        for x, y in self.iterXY():
            if self.grid[y][x]:
                chars[y][x] = '██'
        rows = [''.join(row) for row in chars]
        return '\n'.join(rows)


if __name__=='__main__':
    conway = getBasicConway()
    print("A conway game with a beehive, blinker, and glider:")
    print(conway)
    numSteps = 9
    conway.step(numSteps)
    print(" And after", numSteps, "steps:")
    print(conway)