RM = rm -rf
VENV_DIR = .venv

ACTIVATE = source $(VENV_DIR)/bin/activate
PY = $(VENV_DIR)/bin/python
# Changes for native Windows builds:
ifeq ($(OS),Windows_NT) 
	ACTIVATE = . $(VENV_DIR)/Scripts/activate
	PY = $(VENV_DIR)/Scripts/python.exe
endif
# Can run in two ways: '$(PY) hello.py' or '$(ACTIVATE) && python hello.py'

.PHONY: venv clean-venv run debug shell

clean-venv:
	$(RM) $(VENV_DIR)

venv: $(VENV_DIR)/.pipMarker
$(VENV_DIR)/.venvMarker:
	python -m venv $(VENV_DIR)
	$(PY) -m pip install --upgrade pip setuptools
	touch $@
$(VENV_DIR)/.pipMarker: requirements.txt $(VENV_DIR)/.venvMarker
	$(PY) -m pip install -r requirements.txt
	touch $@

run: export FLASK_APP=hello_app.app
run: venv
	$(ACTIVATE) && python -m flask run

debug: export FLASK_APP=hello_app.app
debug: export FLASK_DEBUG=1
debug: export FLASK_ENV=development
debug: venv
	@$(PY) --version
	$(ACTIVATE) && python -m flask run

conway: venv
	$(ACTIVATE) && python models/conway.py

shell: venv
	$(ACTIVATE) && exec $(notdir $(SHELL))


