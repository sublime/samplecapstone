# A Sample Capstone Project

A simple web application that runs using Make, Python3, and Flask

This project is in dedication to Dr. John Conway (1937-2020), who gave us the "Game of Life" and much more. 

With [Make](https://www.gnu.org/software/make/) and [Python3](https://www.python.org/) installed, the only command needed is `make run` . Then you can go to http://localhost:5000/ to see the website running on your local machine.  

Other targets:

- `make clean-venv` to reset the python venv
- `make debug` to run the flask app in a way that reloads, and shows errors more readily
- `make conway` to see a tiny example of Conway's game of life in the terminal
- `make shell` to enter a shell (with python venv activated)

### Issues: [link](https://code.vt.edu/sublime/samplecapstone/-/issues)

### AGILE board: [link](https://code.vt.edu/sublime/samplecapstone/-/boards)

### Wiki: [link](https://code.vt.edu/sublime/samplecapstone/-/wikis/home)

